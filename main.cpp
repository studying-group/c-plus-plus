// Задание 1. Создание проекта в среде MS Visual Studio с поддержкой OpenMP

#include <iostream>
#include <omp.h>
#include <unistd.h>
#include "math.h"
#include <bits/stdc++.h>

using namespace std;

int THREAD_COUNT = 4;
int MATRIX_SIZE = 12;

// Задание 2. Многопоточная программа «Hello World!»
void task_2 (int thread_cnt) {
    #pragma omp parallel num_threads(thread_cnt)
    {
        cout << "Hello world\n";
    }
}

// Задание 3. Программа «I am!»
void task_3_1 () {
    int thread_cnt;
    cout << endl << endl << "Введите число потоков:" << endl;
    cin >> thread_cnt;

    #pragma omp parallel num_threads(thread_cnt)
    {
        cout << "Hello world from " + to_string(omp_get_thread_num()) + " thread from " + to_string(omp_get_num_threads()) + "\n";
    }
}

void task_3_2 () {
    int thread_cnt;
    cout << endl << endl << "Введите число потоков:" << endl;
    cin >> thread_cnt;

    #pragma omp parallel num_threads(thread_cnt)
    {
        int thread_num = omp_get_thread_num();
        if (thread_num % 2 == 0) {
            cout << "Hello world from " + to_string(omp_get_thread_num()) + " thread from " + to_string(omp_get_num_threads()) + "\n";
        }
    }
}

//Задание 4. Общие и частные переменные в OpenMP: программа «Скрытая ошибка»
void task_4 () {
    int thread_cnt;
    cout << endl << endl << "Введите число потоков:" << endl;
    cin >> thread_cnt;
    int rank;

    #pragma omp parallel num_threads(thread_cnt) private(rank)
    {
        rank = omp_get_thread_num();
        sleep(0.1);
        printf("I am %d thread.\n", rank);
    }
}

// Задание 5. Общие и частные переменные в OpenMP: параметр reduction
void task_5 () {
    // k - количество тредов
    // N - числа от 1..N
    int k, N;
    cout << endl << endl << "Введите число потоков:" << endl;
    cin >> k;
    cout << endl << "Введите количество чисел:" << endl;
    cin >> N;

    int N_left = 1;

    int sum = 0;

    omp_set_num_threads(k);
    #pragma omp parallel reduction(+:sum)
    {
        int left_side;
        int right_side;

        left_side = N_left;
        right_side = N_left + (N - 1) / k + 1;

        if (right_side <= N + 1) {
            for (int i = left_side; i < right_side; i++, N_left++) {
                sum += i;
            }
        }

        printf("[%d]: sum = %d \n", omp_get_thread_num(), sum);
    }

    printf("sum = %d \n", sum);
}


//Задание 6. Распараллеливание циклов в OpenMP: программа «Сумма чисел»
string sumText(int threadNum, int sum) {
    return "[" + to_string(threadNum) + "]: sum = " + to_string(sum);
}

void task_6 () {
    int k, N;
    cout << endl << endl << "Введите число потоков:" << endl;
    cin >> k;
    cout << endl << "Введите количество чисел:" << endl;
    cin >> N;

    int sum = 0;
    string sums[k];
    omp_set_num_threads(k);

    for (int i = 0; i < k; i++) {
        sums[i] = sumText(i, 0);
    }

    #pragma omp parallel for reduction(+:sum)
        for (int i = 1; i <= N; i++) {
            sum += i;
            int threadNum = omp_get_thread_num();
            sums[threadNum] = sumText(threadNum, sum);
        }

    for (int i = 0; i < k; i++) {
        cout << sums[i] << endl;
    }
    printf("sum = %d \n", sum);
}

// Задание 7. Распараллеливание циклов в OpenMP: параметр schedule
void task_7 () {
    int k = 4;
    int N = 10;
    int sum = 0;

    int schedule_static[N];
    int schedule_static_1[N];
    int schedule_static_2[N];
    int schedule_dynamic[N];
    int schedule_dynamic_2[N];
    int schedule_guided[N];
    int schedule_guided_2[N];
    
    for (int i = 0; i < N; i++) {
        schedule_static[i] = -1;
        schedule_static_1[i] = -1;
        schedule_static_2[i] = -1;
        schedule_dynamic[i] = -1;
        schedule_dynamic_2[i] = -1;
        schedule_guided[i] = -1;
        schedule_guided_2[i] = -1;
    }

    omp_set_num_threads(k);
    #pragma omp parallel for schedule(static) reduction(+:sum)
    for (int i = 1; i <= N; i++) {
        sum += i;
        schedule_static[i - 1] = omp_get_thread_num();
        sleep(0.1);
    }

    #pragma omp parallel for schedule(static, 1) reduction(+:sum)
    for (int i = 1; i <= N; i++) {
        sum += i;
        schedule_static_1[i - 1] = omp_get_thread_num();
        sleep(0.1);
    }

    #pragma omp parallel for schedule(static, 2) reduction(+:sum)
    for (int i = 1; i <= N; i++) {
        sum += i;
        schedule_static_2[i - 1] = omp_get_thread_num();
        sleep(0.1);
    }

    #pragma omp parallel for schedule(dynamic) reduction(+:sum)
    for (int i = 1; i <= N; i++) {
        sum += i;
        schedule_dynamic[i - 1] = omp_get_thread_num();
        sleep(0.1);
    }

    #pragma omp parallel for schedule(dynamic, 2) reduction(+:sum)
    for (int i = 1; i <= N; i++) {
        sum += i;
        schedule_dynamic_2[i - 1] = omp_get_thread_num();
        sleep(0.1);
    }

    #pragma omp parallel for schedule(guided) reduction(+:sum)
    for (int i = 1; i <= N; i++) {
        sum += i;
        schedule_guided[i - 1] = omp_get_thread_num();
        sleep(0.1);
    }

    #pragma omp parallel for schedule(guided) reduction(+:sum)
    for (int i = 1; i <= N; i++) {
        sum += i;
        schedule_guided_2[i - 1] = omp_get_thread_num();
        sleep(0.1);
    }



    printf(" № | STATIC | STATIC_1 | STATIC_2 | DYNAMIC | DYNAMIC_2 | GUIDED | GUIDED_2\n");
    for (int i = 0; i < N; i++) {
        printf("%2d | %6d | %8d | %8d | %7d | %9d | %6d | %8d  \n",
               i + 1, schedule_static[i],schedule_static_1[i], schedule_static_2[i],
               schedule_dynamic[i], schedule_dynamic_2[i], schedule_guided[i], schedule_guided_2[i]);
    }

}

// Задание 8. Распараллеливание циклов в OpenMP: программа «Число π»
void task_8 () {
    int eps;
    cout << endl << endl << "Введите точность eps (10^eps):" << endl;
    cin >> eps;

    double iter_count = pow(10.0, eps);
    double inv_iter_count = 1.0 / iter_count;

    double sum = 0.0;
    double x_i;
    double start = omp_get_wtime(), finish;

    omp_set_num_threads(10);
    #pragma omp parallel for private(x_i) reduction(+:sum)
    for (int i = 0; i < (int) iter_count; i++) {
        x_i = inv_iter_count * (i + 0.5);
        sum += 4.0 / (1.0 + x_i * x_i);
    }

    finish = omp_get_wtime();

    printf("pi = %.16f\n", sum * inv_iter_count);
    printf("Time: %fs\n", finish - start);
}

// Задание 9. Распараллеливание циклов в OpenMP: программа «Матрица»
void print_vector(vector<vector<int>> v) {
    printf("Матрица:\n");
    int size = v.size();
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            printf("%d ", v[i][j]);
        }
        printf("\n");
    }
}

void fill_vector(vector<vector<int>> &v, int def = -1, bool isTest = false, int shift = 0) {
    if (isTest) {
        if (0 == def) {
            for (int i = 0; i < MATRIX_SIZE; i++) {
                vector<int> row;
                for (int k = 0; k < MATRIX_SIZE; k++) {
                    row.push_back(0);
                }
                v.push_back(row);
            }
        } else {
            for (int i = 0; i < MATRIX_SIZE; i++) {
                vector<int> row;
                for (int j = 0; j < MATRIX_SIZE; j++) {
                    row.push_back(i + j + shift);
                }
                v.push_back(row);
            }
        }
    } else {
        for (int i = 0; i < MATRIX_SIZE; i++) {
            vector<int> row;
            for (int j = 0; j < MATRIX_SIZE; j++) {
                int t;
                if (0 == def) t = 0; else cin >> t;
                row.push_back(t);
            }
            v.push_back(row);
        }
    }
}

void task_9 (bool isTest = false, int threadCount = 10) {
    if (!isTest) {
        cout << endl << endl << "Введите размерность матриц" << endl;
        cin >> MATRIX_SIZE;
    }

    vector<vector<int>> a;
    vector<vector<int>> b;
    vector<vector<int>> c;
    fill_vector(a, -1, isTest, -5);
    fill_vector(b, -1, isTest, 13);
    fill_vector(c, 0, isTest);

    omp_set_num_threads(threadCount);
    int i = 0;
    int j = 0;
    int m = 0;
    int t = 0;
    #pragma omp parallel for private(m)
    for (m = 0; m < MATRIX_SIZE; m++) {
        #pragma omp parallel for private(i)
        for (i = 0; i < MATRIX_SIZE; i++) {
            #pragma omp parallel for private(j) reduction(+:t)
            for (j = 0; j < MATRIX_SIZE; j++) {
                t += a[i][j] * b[j][m];
                c[i][m] = t;
            }
        }
    }

    if (!isTest) {
        print_vector(c);
    }

    a.clear();
    b.clear();
    c.clear();
    a.shrink_to_fit();
    b.shrink_to_fit();
    c.shrink_to_fit();
}

// Задание 10. Параллельные секции в OpenMP: программа «I'm here»
void task_10 () {
    int k;
    cout << endl << endl << "Введите число потоков:" << endl;
    cin >> k;

    omp_set_num_threads(k);
    #pragma omp parallel
    {
        #pragma omp sections
        {
            #pragma omp section
            {
                printf("[%d]: I'm here in section %d\n", omp_get_thread_num(), 1);
            }
            #pragma omp section
            {
                printf("[%d]: I'm here in section %d\n", omp_get_thread_num(), 2);
            }
            #pragma omp section
            {
                printf("[%d]: I'm here in section %d\n", omp_get_thread_num(), 3);
            }
        }

        printf("[%d]: parallel region\n", omp_get_thread_num());
    }
}

// Задание 11. Гонка потоков в OpenMP: программа «Сумма чисел» с atomic
void task_11 () {
    int k, N;
    cout << endl << endl << "Введите число потоков:" << endl;
    cin >> k;
    cout << endl << "Введите количество чисел:" << endl;
    cin >> N;

    int sum = 0;
    omp_set_num_threads(k);
    #pragma omp parallel for
    for (int i = 1; i <= N; i++) {
        #pragma omp atomic
            sum += i;
        printf("[%d]: sum = %d; i = %d\n", omp_get_thread_num(), sum, i);
    }

    printf("sum = %d\n", sum);
}

// Задание 12. Гонка потоков в OpenMP: программа «Число π» с critical
void task_12 () {
    int eps;
    cout << endl << endl << "Введите точность eps (10^eps):" << endl;
    cin >> eps;

    double iter_count = pow(10.0, eps);
    double inv_iter_count = 1.0 / iter_count;

    double sum = 0.0;
    double x_i;
    double start = omp_get_wtime(), finish;

    omp_set_num_threads(10);
    #pragma omp parallel for private(x_i) shared(sum)
    for (int i = 0; i < (int) iter_count; i++) {
        x_i = inv_iter_count * (i + 0.5);
        #pragma omp critical
        {
            sum += 4.0 / (1.0 + x_i * x_i);
        }
    }

    finish = omp_get_wtime();

    printf("pi = %.16f\n", sum * inv_iter_count);
    printf("Time: %fs\n", finish - start);
}

// Задание 13. Исследование масштабируемости OpenMP-программ
void task_13_1 () {
    printf("test 13\n");

    for (int i = 0; i < 7; i++) {
        int threadCount = i == 0
                          ? 1
                          : i * 2;

        for (int j = 0; j < 2; j++) {
            double start = omp_get_wtime(), finish;
            int N = j == 0
                    ? 100
                    : 1000000;

            printf("%d iters; %d threads\n", j, threadCount);

            for (int k = 0; k < N; k++) {
                task_9(true, threadCount);
            }

            finish = omp_get_wtime();
            printf("[%d:%d]: %f\n", N, threadCount, finish - start);
        }

        printf("\n\n");
    }
}

// *********************************************************************************************************************
// entry point
// *********************************************************************************************************************
int main() {
    int num = 0;

    do {
        cout << endl << endl << "Введите число:" << endl;
        cin >> num;

        switch (num) {
            case    0: break;
            case 1031: task_3_1(); break;
            case 1032: task_3_2(); break;
            case 1040: task_4(); break;
            case 1050: task_5(); break;
            case 1060: task_6(); break;
            case 1070: task_7(); break;
            case 1080: task_8(); break;
            case 1090: task_9(); break;
            case 1100: task_10(); break;
            case 1110: task_11(); break;
            case 1120: task_12(); break;
            case 1131: task_13_1(); break;
            default: task_2(THREAD_COUNT); break;
        }
    } while (1 < num);

    return 0;
}
