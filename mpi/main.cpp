#include <iostream>
#include "/usr/include/x86_64-linux-gnu/mpi/mpi.h"
using namespace std;

bool is_even (int num) {
    return num % 2 == 0;
}

void print_process_count(int size) {
    cout << size << " processes." << endl;
}

void print_i_am_process(int rank) {
    string order = is_even(rank) ? "FIRST" : "SECOND";
    cout << "I am " << rank << ": " << order << endl;
}

int main(int argc, char **argv) {
    MPI_Init(&argc, &argv);

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (rank == 0) {
        print_process_count(size);
    } else {
        print_i_am_process(rank);
    }

    MPI_Finalize();
    return 0;
}